import React, { useEffect, useState } from 'react'
import {
  GlobalStyle,
  ThemeProvider
} from '@datapunt/asc-ui'
import CardCollection from './components/CardCollection'
import CardContainer from './components/CardContainer'
//import Scatterplot from './components/Scatterplot'
//import SmallMults from './components/SmallMults'

//<Scatterplot data={data} category={category} setCategory={setCategory} />
//<SmallMults data={data} />

function App() {
  const [clicked, setClicked] = useState()
  const [data, setData] = useState();

  useEffect(() => {
    fetch('./data/data.json')
      .then(response => response.json())
      .then(json => setData(json))
  }, [])

  return (
    <ThemeProvider>
      <GlobalStyle />
        <CardCollection>
          {data ? data.map((entry, index) => 
            <CardContainer
            key={index}
            name={entry.name}
            index={index}
            clicked={clicked}
            setClicked={setClicked}
            data={entry.data}
            yAxisLabel={entry.yAxisLabel} />
          ) : null}
        </CardCollection>
    </ThemeProvider>
  );
}

export default App;