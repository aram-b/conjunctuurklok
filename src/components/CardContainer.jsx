import React from 'react'
import styled from '@datapunt/asc-core'
import { breakpoint } from '@datapunt/asc-ui'
import CardSmall from './CardSmall'
import CardExpanded from './CardExpanded'

const CardContainer = styled.div`
  margin: 12px;
  width: calc((100% / 3) - 24px);

  //media queries for stacking cards
  @media screen and ${breakpoint('max-width', 'laptop')} {
    width: calc((100% / 2) - 24px);
  }

  @media screen and ${breakpoint('max-width', 'tabletM')} {
    width: calc(100% - 24px);
  }
`

const Card = ({ name, index, clicked, setClicked, data, yAxisLabel }) => {
  const handleClick = () => {
    clicked === index ? setClicked(null) : setClicked(index)
  }

  return (
    <>
      <CardContainer>
        <CardSmall handleClick={handleClick} name={name} data={data} />
        <CardExpanded index={index} clicked={clicked} data={data} yAxisLabel={yAxisLabel} />
      </CardContainer>
    </>
  )
}

export default Card