import React from 'react';
import {
  ResponsiveContainer,
  LineChart,
  Line,
  XAxis
} from 'recharts';

const XAxisTicksAms = ({ x, y, payload }) => (
  <g transform={`translate(${x},${y})`}>
    <text
    textAnchor='middle'
    fill={'#767676'}
    fontSize='14px'>
      {payload.value}
    </text>
  </g>
)

const SparkLine = ({ data, height = 80 }) => (
  <ResponsiveContainer width='100%' height={height} minWidth={0}>
    {data ?
    <LineChart data={data}>
      <XAxis
      stroke='transparent'
      dataKey='name'
      ticks={[data[0].name, data[data.length-1].name]}
      interval='preserveStartEnd'
      tick={<XAxisTicksAms />}
      height={20} />
      <Line
      dataKey='value'
      stroke='#004699'
      strokeWidth='2px'
      dot={false}
      animationDuration={250} />
    </LineChart> :
    null }
  </ResponsiveContainer>
)

export default SparkLine;