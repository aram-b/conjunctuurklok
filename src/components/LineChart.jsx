import React, { useEffect, useRef } from 'react';
import {
  ResponsiveContainer,
  LineChart,
  CartesianGrid,
  Line,
  XAxis,
  YAxis,
  Tooltip
} from 'recharts';
import TooltipAms from './TooltipAms';
import LoadingAnimation from '@bit/alimpens.ams-charts.loading-animation'

const XAxisTicksAms = ({ x, y, payload }) => (
  <g transform={`translate(${x},${y})`}>
    <text
    dy={20}
    textAnchor='middle'
    fill={'white'}
    fontSize='14px'>
      {payload.value}
    </text>
  </g>
)

const CustomizedLabel = ({ viewBox, value }) => {
  const labelRef = useRef(null);

  useEffect(() => {
    const text = labelRef.current.getElementsByTagName('text')[0];
    const rect = labelRef.current.getElementsByTagName('rect')[0];
    const bbox = text.getBBox();

    rect.setAttribute('width', bbox.width + 8);
    rect.setAttribute('height', bbox.height);
    rect.setAttribute('transform', `translate(0 -${bbox.height / 2})`);
  })

  return (
    <g ref={labelRef}>
      <rect x={viewBox.width - 4} y={viewBox.y} fill='#00A03C' />
      <text x={viewBox.width} y={viewBox.y} dy={5} fontSize='14px' fill='white'>
        <tspan>{value}</tspan>
      </text>
    </g>
  )
}

const LineChartAms = ({ data, height = 320, yAxisLabel = '%' }) => {
  return (
    <ResponsiveContainer width='100%' height={height}>
      {data ?
      <LineChart data={data} margin={{ right: 5, top: 12 }}>
        <CartesianGrid vertical={false} stroke='#e6e6e6' />
        <XAxis
        dataKey='name'
        tickSize={0}
        tick={<XAxisTicksAms />}
        stroke='black'
        strokeWidth='2px'/>
        <Tooltip
        position={{ y: height / 2 }}
        offset={10}
        animationDuration={250}
        content={<TooltipAms yAxisLabel={yAxisLabel} />}
        cursor={{ fill: '#e6e6e6', strokeWidth: '2px', strokeDasharray: '2,2' }} />
        <Line
        dataKey='value'
        stroke='white'
        strokeWidth='2px'
        dot={false}
        animationDuration={350} />
        <YAxis
        width={45}
        axisLine={false}
        tickSize={0}
        tick={{ fontSize: '14px', fill: 'white', transform: 'translate(-4, 0)' }}
        label={{ value: yAxisLabel, content: <CustomizedLabel /> }} />
      </LineChart> :
      <LoadingAnimation />}
    </ResponsiveContainer>
  )
}

export default LineChartAms;