import React from 'react'
import Heading from '@datapunt/asc-ui/lib/components/Heading'
import styled from 'styled-components'
import {
  LineChart, Line, XAxis
} from 'recharts';

const StyledContainer = styled.div`
  margin-top: 24px;
  margin-bottom: 24px;
  padding: 12px;
`

const StyledCard = styled.div`
  display: inline-block;
  width: 200px;
  vertical-align: top;
  margin-top: 8px;
`

const StyledValue = styled.div`
  font-size: 36px;
  font-weight: bold;
  color: ${props => props.value > 0 ? 'green' : 'red'}
`

const ArrowDown = styled.div`
  display: inline-block;
  width: 0;
  height: 0;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-top: 10px solid red;
  margin-right: 8px;
  vertical-align: middle;
`

const ArrowUp = styled.div`
  display: inline-block;
  width: 0; 
  height: 0; 
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid green;
  margin-right: 8px;
  vertical-align: middle;
`

const StyledInfo = styled.div`
  color: grey;
  font-size: 14px;
`

const SmallMults = ({ data }) => {
  const categories = data ? [...new Set(data.map(item => item.name))] : null;
  return (
    <>
      {categories ? categories.map(entry => {
        const filteredData = data.filter(item => item.name === entry)
        const lastEntry = filteredData.find(item => item.date === 'dec-19')
        return (
          <StyledContainer key={entry}>
            <StyledCard>
              <StyledValue value={lastEntry.groei}>
                {lastEntry.groei < 0 ? <ArrowDown /> : <ArrowUp />}
                {lastEntry.groei}
              </StyledValue>
              <Heading forwardedAs='h2'>{entry}</Heading>
              <StyledInfo>{lastEntry.date}</StyledInfo>
            </StyledCard>
            <LineChart style={{display: 'inline-block'}} width={600} height={100} data={filteredData}>
              <Line dataKey="groei" stroke="#004699" strokeWidth={2} dot={false}/>
              <XAxis
              dataKey="date"
              orientation='top'
              axisLine={false} />
            </LineChart>
          </StyledContainer>
        )
      }) : null}
    </>
  )
}

export default SmallMults