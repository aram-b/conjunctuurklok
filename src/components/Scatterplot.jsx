import React from 'react'
import { themeColor } from '@datapunt/asc-ui'
import styled from 'styled-components'
import {
  ScatterChart, Scatter, XAxis, YAxis, Tooltip, Cell
} from 'recharts'

const colors = {
  "1":"#ec0000",
  "2":"#FF9100",
  "3":"#FFE600",
  "4":"#00A03C"
}

const StyledTooltip = styled.div`
  background-color: white;
  border: 1px solid #979797;
  border-radius: 1px;
  box-shadow: 0 2px 4px 0 #979797;
  padding: 8px 12px;
  min-width: 120px;
`

const StyledTooltipTitle = styled.p`
  font-size: 18px;
  color: ${themeColor('tint', 'level5')};
  margin: 0;
  margin-bottom: 8px;
`

const StyledTooltipList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`

const StyledTooltipItem = styled.li`
  margin-top: 2px;
  margin-bottom: 2px;
`

const TooltipAms = ({ active, payload }) => {
  if (active && payload[0]) {
    const cat = payload[0].payload.category
    return (
      <StyledTooltip>
        <StyledTooltipTitle>{payload[0].payload.date}</StyledTooltipTitle>
        <StyledTooltipList>
          <StyledTooltipItem>{`Categorie: ${payload[0].payload.name}`}</StyledTooltipItem>
          <StyledTooltipItem>{`Kwadrant: ${
            cat === 1 ? 'Laagconjunctuur' :
            cat === 2 ? 'Neergang' : 
            cat === 3 ? 'Herstel' : 
            cat === 4 ? 'Hoogconjunctuur' : null}`}</StyledTooltipItem>
          <StyledTooltipItem>{`Positie: ${payload[0].payload.rank}`}</StyledTooltipItem>
          <StyledTooltipItem>{`Sterkte: ${payload[0].payload.strength}`}</StyledTooltipItem>
        </StyledTooltipList>
      </StyledTooltip>
    )
  } else return null;
}

const CustomizedDot = ({ x, y, fill, stroke, rank }) => {
  const add = rank > 0 ? 7 : -7
  return (
    <rect x={x-2} y={y-2+add} width={12} height={12} fill={fill} stroke={stroke} />
  );
}

const Scatterplot = ({ data, category, setCategory }) => (
  <ScatterChart width={800} height={400}>
    <XAxis
    dataKey="date"
    allowDuplicatedCategory={false}
    orientation='top'
    axisLine={false} />
    <YAxis
    type="number"
    dataKey="rank"
    axisLine={false}
    interval='preserveStartEnd'
    width={200} />
    <Tooltip cursor={false} content={<TooltipAms />} />
    <Scatter
    data={data}
    shape={<CustomizedDot />}
    onMouseEnter={x => setCategory(x.name)}
    onMouseLeave={x => setCategory(null)}>
      {data ? data.map((entry, index) => 
      <Cell
      key={`cell-${index}`}
      fill={colors[entry.category]}
      stroke={entry.name === category ? 'black' : null} />) :
      null}
    </Scatter>
  </ScatterChart>
)

export default Scatterplot