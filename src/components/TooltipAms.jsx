import React from 'react';
import styled from 'styled-components';
import { themeColor } from '@datapunt/asc-ui';

const StyledTooltip = styled.div`
  background-color: white;
  border: 1px solid #979797;
  border-radius: 1px;
  box-shadow: 0 2px 4px 0 #979797;
  padding: 8px 12px;
  min-width: 120px;
`

const StyledTooltipTitle = styled.p`
  font-size: 18px;
  color: ${themeColor('tint', 'level5')};
  margin: 0;
  margin-bottom: 8px;
`

const StyledTooltipList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`

const StyledTooltipItem = styled.li`
  margin-top: 2px;
  margin-bottom: 2px;
`

const StyledTooltipValue = styled.span`
  font-weight: bold;
  font-size: 18px;
  color: black;
`

const StyledTooltipInfo = styled.span`
	font-size: 14px;
	color: ${themeColor('tint', 'level5')};
	margin-bottom: 4px;
`

const TooltipAms = ({ active, payload, yAxisLabel }) => {
  if (active && payload[0]) {
    return (
      <StyledTooltip>
        <StyledTooltipTitle>{payload[0].payload.name}</StyledTooltipTitle>
        <StyledTooltipList>
            <StyledTooltipItem>
              <StyledTooltipValue>{Math.round(payload[0].payload.value * 10) / 10}</StyledTooltipValue>
              <StyledTooltipInfo>{` ${yAxisLabel}`}</StyledTooltipInfo>
            </StyledTooltipItem>
        </StyledTooltipList>
      </StyledTooltip>
    )
  } else return null;
}

export default TooltipAms;