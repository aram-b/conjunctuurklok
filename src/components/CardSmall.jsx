import React from 'react'
import { themeColor, Heading } from '@datapunt/asc-ui'
import styled from '@datapunt/asc-core'
import SparkLine from './SparkLine'

const StyledCardSmall = styled.button`
  border: none;
  margin: 0;
  background-color: ${themeColor('tint', 'level3')};
  width: 100%;
  padding: 20px;
  position: relative;
  cursor: pointer;
  transition: transform 0.15s ease-in-out;
  transition: background-color 0.1s ease-in-out 0s;
  &:hover{
    background-color: ${themeColor('tint', 'level4')};
  }
  &:focus{
    outline: 3px solid ${themeColor('support', 'focus')};
  }
`

const ChartContainer = styled.div`
  display: flex;
  flex-orientation: row;
  align-items: center;
  pointer-events: none;
`

const Value = styled.div`
  position: relative;
  font-size: 28px;
  font-weight: bold;
  margin-right: 12px;
  margin-left: 32px;
  color: ${({ change }) => change > 0 ? '#00A03C' : change < 0 ? '#ec0000' : 'black'};

  &::after {
    content: '';
    position: absolute;
    display: inline-block;
    width: ${({ change }) => change === 0 ? '10px' : '0'};
    height: ${({ change }) => change === 0 ? '10px' : '0'};
    border-radius: ${({ change }) => change === 0 ? '50%' : '0'};
    background-color: ${({ change }) => change === 0 ? 'black' : 'transparent'};
    left: 0;
    top: calc(50% - 4px);
    margin-left: -32px;
    border-left: ${({ change }) => change === 0 ? 'none' : '10px solid transparent'};
    border-right: ${({ change }) => change === 0 ? 'none' : '10px solid transparent'};
    border-bottom: ${({ change }) => change > 0 ? '10px solid #00A03C' : 'none'};
    border-top: ${({ change }) => change < 0 ? '10px solid #ec0000' : 'none'};
  }
`

const CardSmall = ({ handleClick, name, data }) => {
  const newValue = data[data.length-1].value
  const oldValue = data[0].value
  const change = ((newValue-oldValue)/oldValue)*100

  return (
    <StyledCardSmall onClick={handleClick}>
      <Heading forwardedAs='h3' gutterBottom={0}>{name}</Heading>
      <ChartContainer>
        <Value change={change}>{`${Math.round(change * 10) / 10}%`}</Value>
        <SparkLine data={data}/>
      </ChartContainer>
    </StyledCardSmall>
  )
}

export default CardSmall