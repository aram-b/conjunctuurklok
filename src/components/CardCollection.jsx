import React from 'react'
import { breakpoint } from '@datapunt/asc-ui'
import styled from '@datapunt/asc-core'

const StyledCardCollection = styled.div`
  padding: 15px;
  display: flex;
  flex-flow: row wrap;

  @media screen and ${breakpoint('max-width', 'tabletS')} {
    padding: 0;
  }
`

const CardCollection = ({ children }) => (
  <StyledCardCollection>
    {children}
  </StyledCardCollection>
)

export default CardCollection