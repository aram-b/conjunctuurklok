import React from 'react'
import { Paragraph, Row, Column } from '@datapunt/asc-ui'
import styled from '@datapunt/asc-core'
import { breakpoint } from '@datapunt/asc-ui'
import LineChart from './LineChart'

const StyledCardExpanded = styled.div`
  position: relative;
  background-color: #00A03C;
  color: white;
  display: ${({ show }) => show ? 'block' : 'none'};
  margin-top: 24px;
  padding-top: 24px;
  padding-bottom: 24px;

  &:after{
    bottom: 100%;
    border: solid transparent;
    content: '';
    height: 0;
    width: 0;
    position: absolute;
    left: 50%;
    pointer-events: none;
    border-color: rgba(0, 0, 0, 0);
    border-bottom-color: #00A03C;
    border-width: 16px;
    margin-left: -16px;
  }

  //with 3 cards in a row
  @media screen and ${breakpoint('min-width', 'laptop')} {
    width: calc(300% + 48px);
    margin-left: ${({ index }) =>
      index === 1 || index === 4 || index === 7 ? 'calc(-100% - 24px)' : 
      index === 2 || index === 5 || index === 8 ? 'calc(-200% - 48px)' : 
      'auto'
    };
    &:after{
      left: ${({ index }) =>
        index === 0 || index === 3 || index === 6 ? '16.5%' : 
        index === 2 || index === 5 || index === 8 ? '82.5%' : 
        '50%'
      };
    }
  }

  //with 2 cards in a row
  @media screen and ${breakpoint('min-width', 'tabletM')} and ${breakpoint('max-width', 'laptop')} {
    width: calc(200% + 24px);
    margin-left: ${({ index }) => 
      index === 1 || index === 3 || index === 5 || index === 7 ? 'calc(-100% - 24px)' :
      'auto'
    };
    &:after{
      left: ${({ index }) => 
        index === 0 || index === 2 || index === 4 || index === 6 || index === 8 ? '25%' :
        '75%'
      };
    }
  }
`

const StyledParagraph = styled(Paragraph)`
  @media screen and ${breakpoint('max-width', 'tabletM')} {
    margin-top: 24px;
  }
`

const InfoText = styled(Paragraph)`
  font-size: 14px;
`

const textPlaceholder = 'Hier kan iets van uitleg over de variabele komen. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'

const CardExpanded = ({ index, clicked, data, yAxisLabel }) => (
  <StyledCardExpanded index={index} show={clicked === index ? true : false}>
    <Row valign='flex-start'>
      <Column
      order={{ small: 1, medium: 1, big: 2, large: 2, xLarge: 2 }}
      span={{ small: 1, medium: 2, big: 3, large: 6, xLarge: 6 }}>
        <LineChart data={data} yAxisLabel={yAxisLabel} />
      </Column>
      <Column
      wrap
      order={{ small: 2, medium: 2, big: 1, large: 1, xLarge: 1 }}
      span={{ small: 1, medium: 2, big: 3, large: 6, xLarge: 6 }}>
        <StyledParagraph>{textPlaceholder}</StyledParagraph>
        <InfoText>{`Wordt maandelijks bijgewerkt. Laatste update: ${data[data.length-1].name}`}</InfoText>
      </Column>
    </Row>
  </StyledCardExpanded>
)

export default CardExpanded